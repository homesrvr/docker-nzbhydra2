FROM debian:bookworm-slim

# Env vars
ENV NZBHYDRA2_VER="empty"
ENV NZBHYDRA_DISABLE_WRAPPER_CHECK="true"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
WORKDIR /app/nzbhydra2
RUN \
    apt-get update && \
    apt-get install -y jq unzip curl python3 && \
    NZBHYDRA2_VERSION=$(curl -sX GET 'https://api.github.com/repos/theotherp/nzbhydra2/releases/latest' | jq -r .tag_name) && \
    NZBHYDRA2_VER=${NZBHYDRA2_VERSION#v} && \
    export NZBHYDRA2_VER && \
    echo "Downloading https://github.com/theotherp/nzbhydra2/releases/download/v${NZBHYDRA2_VER}/nzbhydra2-${NZBHYDRA2_VER}-amd64-linux.zip ..." && \
    curl -s -L -o /tmp/nzbhydra2.zip "https://github.com/theotherp/nzbhydra2/releases/download/v${NZBHYDRA2_VER}/nzbhydra2-${NZBHYDRA2_VER}-amd64-linux.zip"  && \
    unzip /tmp/nzbhydra2.zip -d /app/nzbhydra2 && \
    chmod +x /app/nzbhydra2/core && \
    rm -rf "/tmp/*" "/var/lib/apt/lists/*" "/var/tmp/*" "/var/cache/apk/*" "/app/nzbhydra/upstart" "/app/nzbhydra/rc.d" "/app/nzbhydra/sysv"

RUN mkdir -p /config

EXPOSE 5076
VOLUME /config
VOLUME /downloads

# Healthcheck to verify nzbhydra2 is running and responding on port 5076 every minute. 
# Uses curl to ping the /actuator/health/ping endpoint and checks response.
HEALTHCHECK --interval=1m --timeout=5s --start-period=3m \
    CMD /usr/bin/curl -fsSL "http://localhost:5076/actuator/health/ping" && echo "OK" || exit 1

#ENTRYPOINT [ "/app/nzbhydra2/nzbhydra2", "--nobrowser", "--datafolder /config" ]
CMD [ "/usr/bin/python3", "/app/nzbhydra2/nzbhydra2wrapperPy3.py", "--nobrowser", "--datafolder", "/config" ]

# Container labels
ARG BUILD_DATE
ARG BUILD_REF
LABEL org.opencontainers.image.authors = "cusoon"
LABEL org.opencontainers.image.source = "https://gitlab.com/homesrvr/docker-nzbhydra2"
LABEL org.label-schema.build-date=${BUILD_DATE}
LABEL org.label-schema.vcs-ref=${BUILD_REF} 
LABEL org.label-schema.name="nzbhydra2"
LABEL org.label-schema.schema-version="$NZBHYDRA2_VER"
LABEL org.label-schema.url="https://github.com/theotherp/nzbhydra2" 
